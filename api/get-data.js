let data = {
    "degrees": [
        {
            "code": "WBD",
            "name": "Web Design & Development",
            "learning_outcomes": [
                "To understand programming",
                "To gain knowledge of operating systems"
            ],
            "academic": "Toby Essex"
        },
        {
            "code": "PRE",
            "name": "Primary Education",
            "learning_outcomes": [
                "To develop an understanding for teaching in key state 1 & 2",
                "To gain QTS knowledge"
            ],
            "academic": "Micheal Bird"
        },
        {
            "code": "CN",
            "name": "Child Nursing",
            "learning_outcomes": [
                "Learning Outcome 1",
                "Learning Outcome 2"
            ],
            "academic": "Joshua Essex"
        },
        {
            "code": "AD",
            "name": "Adult Nursing",
            "learning_outcomes": [],
            "academic": "Kate Pyatt"
        }
    ],
    "modules": [
        {
            "code": "CIS1000",
            "name": "Server & Client Side Scripting",
            "hours": 60,
            "learning_outcomes": [
                "To gain knowledge of PHP & JavaScript",
                "To learn frameworks like Angular and Laravel"
            ],
            "credits": 20,
            "degree_code": "WBD",
            "academic": "Toby Essex"
        },
        {
            "code": "CIS1001",
            "name": "Web Application Development",
            "hours": 60,
            "learning_outcomes": [
                "To gain knowledge of continuous integration",
                "To learn frameworks like Jenkins and Docker"
            ],
            "credits": 20,
            "degree_code": "WBD",
            "academic": "Joshua Essex"
        },
        {
            "code": "PRE1000",
            "name": "Personal Portfolio Development",
            "hours": 60,
            "learning_outcomes": [
                "To gain knowledge of how to develop a professional portfolio"
            ],
            "credits": 20,
            "degree_code": "PRE",
            "academic": "Katie Hughes"
        },
        {
            "code": "DA",
            "name": "Data Anaylsis",
            "learning_outcomes": [
                "Learning Python"
            ],
            "academic": "Rachel Britton",
            "hours": "60",
            "credits": "20",
            "degree_code": "WBD"
        }
    ],
    "assessments": [
        {
            "number": "CW1",
            "title": "Coursework 1",
            "covered_learning_outcomes": [
                "To gain knowledge of PHP & JavaScript"
            ],
            "volume": 50,
            "weight": 20,
            "submission_date": "29/01/19",
            "module_code": "CIS1000"
        },
        {
            "number": "CW2",
            "title": "Coursework 2",
            "covered_learning_outcomes": [
                "To learn frameworks like Angular and Laravel"
            ],
            "volume": 50,
            "weight": 80,
            "submission_date": "08/05/19",
            "module_code": "CIS1000"
        },
        {
            "number": "CW1",
            "title": "Coursework 1",
            "covered_learning_outcomes": [
                "Learning C++"
            ],
            "volume": "50",
            "weight": "20",
            "module_code": "CIS1001"
        },
        {
            "number": "V",
            "title": "Viva",
            "covered_learning_outcomes": [
                "To gain QTS knowledge",
                "To gain knowledge of how to develop a professional portfolio",
                "To develop an understanding for teaching in key state 1 & 2"
            ],
            "volume": "100",
            "weight": "100",
            "module_code": "PRE1000"
        }
    ],
    "academics": [
        "Toby Essex",
        "Joshua Essex",
        "Nathan Newham",
        "Nathan Pope",
        "Micheal Bird",
        "Kate Pyatt",
        "Rachel Britton",
        "Katie Hughes"
    ]
};


exports.handler = function(event, context, callback) {
    callback(null, {
        statusCode: 200,
        body: JSON.stringify(data)
    });
};
