const express = require("express");
const serverless = require("serverless-http");
const app = express();
const bodyParser = require("body-parser");
const fs = require("fs");
const cors = require("cors");

app.use(cors());
app.use(bodyParser.json());

app.get("/data", (req, res) => {
    // fs.writeFile("../data.json", JSON.stringify(data));

    res.json(JSON.parse(fs.readFileSync("../data.json").toString()));
});

app.post("/add-degree", (req, res) => {
    let data = JSON.parse(fs.readFileSync("../data.json").toString());

    data.degrees.push(req.body);

    fs.writeFile("../data.json", JSON.stringify(data));

    res.json({
        success: {
            message: "Degree Added",
        }
    })
});

app.post("/add-module", (req, res) => {
    let data = JSON.parse(fs.readFileSync("../data.json").toString());

    data.modules.push(req.body);

    fs.writeFile("../data.json", JSON.stringify(data));

    res.json({
        success: {
            message: "Module Added",
        }
    })
});

app.post("/add-assessment", (req, res) => {
    let data = JSON.parse(fs.readFileSync("../data.json").toString());

    data.assessments.push(req.body);

    fs.writeFile("../data.json", JSON.stringify(data));

    res.json({
        success: {
            message: "Assessment Added",
        }
    })
});

// module.exports.handler = serverless(app);

exports.handler = function(event, context, callback) {
    callback(null, {
        statusCode: 200,
        body: JSON.parse(fs.readFileSync("data.json").toString())
    });
};

app.listen(8000, () => console.log("Listening on port 8000!"));
