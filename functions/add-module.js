exports.handler = function(event, context, callback) {
    const fs = require("fs");

    let data = JSON.parse(fs.readFileSync("data.json").toString());

    data.modules.push(event.body);

    fs.writeFile("data.json", JSON.stringify(data));

    callback(null, {
        statusCode: 201,
        body: "Module Added"
    });
};
